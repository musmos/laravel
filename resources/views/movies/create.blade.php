<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif


	<form action="/movies/store" method="post">
	@csrf

		title
		<input type="text" name="title">
		category
		<input type="text" name="category">
		ratings
		<input type="number" name="ratings">
		<input type="submit" name="submit" value="Add">
	</form>

</body>
</html>