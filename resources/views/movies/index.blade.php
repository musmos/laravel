<!DOCTYPE html>
<html>
<head>
	<meta name="_token" content="{{ csrf_token() }}">
    <title>Cinema 1</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>



<style type="text/css">
	td{border: black solid 1px;}
	th{border: black solid 1px;}
	tr{border: black solid 1px;}
</style>


</head>
<body>





<div class="container">
    <div class="row">
    <div class="panel panel-default">
    <div class="panel-heading">

<h1>Cinema1</h1>
</div>
 <div class="panel-body">
   <div class="form-group">
<a href="/movies/create">Add Movie</a>
<br>
Genres
<a href="/movies/filter/{{"Action"}}">Action</a>
<a href="/movies/filter/{{"Sci-Fi"}}">Sci-Fi</a>
<a href="/movies/filter/{{"Comedy"}}">Comedy</a>
<a href="/movies">All</a>

<br>
{{-- Search --}}
 {{-- <input type="text" class="form-controller" id="index" name="index"></input> --}}

 <form action="/search" method="get" >

    {{-- {{ csrf_field() }} --}}
    <div class="input-group">
        <input type="search" class="form-control" name="search"
            placeholder="Search movie"> 
            <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
             <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</form>


    </div>
<table class="table table-bordered table-hover">
	
	<thead>
		<tr>
			<th>Title</th>
			<th>Category</th>
			<th>Ratings</th>
			<th>Actions</th>
		</tr>

	</thead>
	<tbody>

		


	@foreach ($movies as $movie)
	<tr>
		<td>{{$movie->title}}</td>
		<td>{{$movie->category}}</td>
		<td>{{$movie->ratings}}</td>
		<td><a href="/movies/edit/{{$movie->id}}" >Edit</a>&nbsp; <a href="/movies/delete/{{$movie->id}}" >Delete</a></td>
		

	</tr>	
	@endforeach
	</tbody>
</table>
	</div>
</div>
</div>
</div>



</body>
</html>