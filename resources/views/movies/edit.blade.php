<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif
{{-- /movies/update/{{$movies ?? ''->id}} --}}
	<form action="/movies/update/{{$movies->id}}" method="post">
	@csrf
		<input type="hidden" name="id" value="{{$movies->id}}">
		title
		<input type="text" name="title">
		category
		<input type="text" name="category">
		ratings
		<input type="number" name="ratings" >
		<input type="submit" name="submit" value="update">
	</form>

</body>
</html>