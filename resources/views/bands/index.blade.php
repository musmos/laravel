<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<table>
	
<thead>
	<tr>
		<th>Name</th>
		<th>Date Formed</th>
		<th>Action</th>

	</tr>

</thead>
<tbody>
		@foreach($bands as $band)

	<tr>
		<td>{{$band->name}}</td>
		<td>{{\Carbon\Carbon::parse($band->date_formed)->toFormattedDateString()}}</td>
		<td>
			<a href="/bands/show/{{$band->id}}">View</a>
		</td>
	</tr>
		@endforeach

</tbody>

</table>
 
</body>
</html>