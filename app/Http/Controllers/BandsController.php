<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Band;
class BandsController extends Controller
{
    public function index(){
    	$bands = Band::get();
    


    	return view('/bands.index', compact('bands'));
    

    }

    public function show(Band $band){
    	$bands = Band::get();

    	return view('/bands/show',compact('bands'))->with('bandz' , $band);
    }
}
