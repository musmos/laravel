<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Movie;
class MoviesController extends Controller
{
    //
    public function search(Request $request){


        $search = $request->get('search');
        // if(!is_null($search)){
        // dd($search);
         $movies = DB::table('movies')
         ->where('title','like','%'.$search.'%')
         ->paginate(5);


        return view('movies.index',['movies' => $movies]);
        // if(is_null($search)){
            // return "null";

        // }
        // if(count($movies) > 0){
            // return "if statement";
        // }
        // }
        // else

            // return "error";
            // return view('movies.index')->with(['msg', 'The Message']);
            // return view('movies.index')->with(['msg','themessage']);
         // dd($posts);
    }




    public function filter($genre){
        // $movies -> each(function($movie))

        $movies = Movie::where('category',$genre)->get();

         return view('movies.index',['movies'=>$movies]);

    }

    public function index(){
    	// $movies = DB::table('movies')->get();
        $movies = Movie::all();
        // $movies -> each(function($movie))
    	 return view('movies.index',['movies'=>$movies]);
    }

    public function create(){

    	return view('movies.create');
    }

    public function store(){
    	request()->validate(['title'=>'required','category'=>'required','ratings'=>'required']);

        $movies = Movie::create([
        'title'=>request()->title,'category'=>request()->category,'ratings'=>request()->ratings
        ]);

    	// $movies = DB::table('movies')
    	// ->insert([
    		// ['title'=>request()->title,'category'=>request()->category,'ratings'=>request()->ratings]
    	// ]);

    	// return redirect()->back();
    	  return redirect('/movies');

    }

    public function update($id){
    	request()->validate(['title'=>'required','category'=>'required','ratings'=>'required']);

        $movies = Movie::where('id',$id)->first();
        $movies->title=request()->title;
        $movies->category=request()->category;
        $movies->ratings=request()->ratings;
        $movies->save();

    	// $movies = DB::table('movies')
    	// ->where('id',$id)
    	// ->update([
    		// 'title'=>request()->title,'category'=>request()->category,'ratings'=>request()->ratings
    	// ]);

    	return redirect('/movies');
    }

    public function edit($id){

        $movies = Movie::find($id);
    	return view('movies.edit')->with('movies',$movies);

    }

    public function delete($id){

    // $movies = DB::table('movies')
     // ->where('id',$id)
     // ->delete();
    $movies = Movie::where('id',$id)->first();
    $movies->delete();


     return redirect('/movies');

    }

   


}
