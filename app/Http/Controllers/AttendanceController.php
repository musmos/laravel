<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class AttendanceController extends Controller
{
    //


    public function create(){
    	
    	return view('Attendances/create');


    }

    public function store(){
    	$attendances = DB::table('attendances')
    	->insert([
    		['date'=> request()->date,'employee'=> request()->employee,'time_in'=> request()->time_in,]
    	]);
        return redirect()->back();
    
    }
    
}
